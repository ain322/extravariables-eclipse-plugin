package extravariables;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

public class Activator extends Plugin {

	private static BundleContext ctx;

	static BundleContext getContext() {
		return ctx;
	}

	public void start(BundleContext ctx) throws Exception {
		Activator.ctx = ctx;
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.ctx = null;
	}

}
