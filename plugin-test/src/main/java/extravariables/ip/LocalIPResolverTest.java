package extravariables.ip;

import static org.junit.Assert.assertEquals;

import java.net.InetAddress;

import org.junit.Before;
import org.junit.Test;

import extravariables.network.LocalIPResolver;

public class LocalIPResolverTest {

	LocalIPResolver resolver;

	@Before
	public void setup() {
		resolver = new LocalIPResolver();
	}

	@Test
	public void should_return_local_ip_address() throws Exception {
		assertEquals(InetAddress.getLocalHost().getHostAddress(), resolver.resolveValue(null, null));
	}

}
